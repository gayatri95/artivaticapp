package com.example.artivaticapp

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.ml.vision.FirebaseVision
import com.google.firebase.ml.vision.common.FirebaseVisionImage
import com.google.firebase.ml.vision.text.FirebaseVisionText
import kotlinx.android.synthetic.main.activity_text_recognizer.*


class TextRecognizer : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_text_recognizer)

        var bmp: Bitmap? = null
        if (intent.hasExtra("image")){
            val filename = intent.getStringExtra("image")
            try {
                val image = this.openFileInput(filename)
                bmp = BitmapFactory.decodeStream(image)
                image.close()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        detectImage(bmp)
    }

    private fun detectImage(imageBitmap: Bitmap?) {

        et_text.text = ""
        val image = imageBitmap?.let { FirebaseVisionImage.fromBitmap(it) }
        val textRecognizer = FirebaseVision.getInstance().onDeviceTextRecognizer
        if (image != null) {
            textRecognizer.processImage(image)
                .addOnSuccessListener { fireBaseVisionText -> processText(fireBaseVisionText) }
                .addOnFailureListener { }
        }
    }

    private fun processText(fireBaseVisionText: FirebaseVisionText?) {

        val blocks = fireBaseVisionText?.textBlocks
        when {
            blocks?.size == 0 -> Toast.makeText(this, getString(R.string.please_capture_valid_image), Toast.LENGTH_LONG).show()
            else -> {
                for (block in fireBaseVisionText?.textBlocks!!) {
                    val blockText = block.text
                    et_text.append(blockText + "\n")
                }
            }
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }

}
