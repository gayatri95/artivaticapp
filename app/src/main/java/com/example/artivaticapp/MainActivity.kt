package com.example.artivaticapp

import android.Manifest
import android.Manifest.permission.READ_EXTERNAL_STORAGE
import android.app.Activity
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.ImageDecoder
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import kotlinx.android.synthetic.main.activity_main.*
import java.io.IOException


class MainActivity : AppCompatActivity(), View.OnClickListener {

    private val GALLERY = 1
    private val CAMERA = 2

    private val PERMISSION_REQUEST_CODE: Int = 101
    var imageBitmap: Bitmap? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btn_capture_image?.setOnClickListener(this)
        btn_show_text?.setOnClickListener(this)

    }

    override fun onClick(view: View?) {

        when (view?.id) {

            R.id.btn_capture_image -> {
                when {
                    checkPermission() -> showPictureDialog()
                    else -> requestPermission()
                }
            }

            R.id.btn_show_text -> {

                try {
                    val filename = "bitmap.png"
                    val stream = this.openFileOutput(filename, Context.MODE_PRIVATE)
                    imageBitmap?.compress(Bitmap.CompressFormat.PNG, 100, stream)

                    stream.close()
//                    imageBitmap?.recycle()

                    val intent = Intent(this, TextRecognizer::class.java)
                    intent.putExtra("image", filename)
                    startActivity(intent)
                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }
        }
    }


    private fun checkPermission(): Boolean {
        return (ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA) ==
                PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(
            this,
            android.Manifest.permission.READ_EXTERNAL_STORAGE
        ) == PackageManager.PERMISSION_GRANTED)
    }

    private fun requestPermission() {
        ActivityCompat.requestPermissions(
            this, arrayOf(READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA),
            PERMISSION_REQUEST_CODE
        )
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {

        when (requestCode) {
            PERMISSION_REQUEST_CODE -> {

                when {
                    (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                            && grantResults[1] == PackageManager.PERMISSION_GRANTED -> showPictureDialog()
                    else -> Toast.makeText(this, getString(R.string.permission_denied), Toast.LENGTH_SHORT).show()
                }
                return
            }
        }
    }


    private fun showPictureDialog() {

        val pictureDialog = AlertDialog.Builder(this)
        pictureDialog.setTitle(getString(R.string.select_action))
        val pictureDialogItems =
            arrayOf(getString(R.string.select_photo_from_gallery), getString(R.string.capture_photo_from_camera))
        pictureDialog.setItems(pictureDialogItems,
            DialogInterface.OnClickListener { _, which ->
                when (which) {
                    0 -> choosePhotoFromGallery()
                    1 -> takePhotoFromCamera()
                }
            })
        pictureDialog.show()
    }

    private fun takePhotoFromCamera() {

        val intent = Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE)
        startActivityForResult(intent, CAMERA)

    }

    private fun choosePhotoFromGallery() {

        val galleryIntent = Intent(
            Intent.ACTION_PICK,
            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        )

        startActivityForResult(galleryIntent, GALLERY)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when {
            resultCode == Activity.RESULT_CANCELED -> return
            requestCode == GALLERY && resultCode == Activity.RESULT_OK -> data?.let {
                val contentURI = it.data
                try {
                    val bitmap = MediaStore.Images.Media.getBitmap(this.contentResolver, contentURI)
                    imageBitmap = bitmap
                    iv_image.setImageBitmap(bitmap)

                    //getBitmap(ContentResolver!, Uri!): Bitmap!' is deprecated...instead of this we can use following code but it requires API 28 and our current is 23
//                    when {
//                        Build.VERSION.SDK_INT < 28 -> {
//                            val bitmap = MediaStore.Images.Media.getBitmap(
//                                this.contentResolver,
//                                contentURI
//                            )
//                            iv_image.setImageBitmap(bitmap)
//                        }
//                        else -> {
//                            val source = contentURI?.let { it1 -> ImageDecoder.createSource(this.contentResolver, it1) }
//                            val bitmap = source?.let { it1 -> ImageDecoder.decodeBitmap(it1) }
//                            iv_image.setImageBitmap(bitmap)
//                        }
//                    }

                } catch (e: IOException) {
                    e.printStackTrace()
                    Toast.makeText(this@MainActivity, getString(R.string.failed), Toast.LENGTH_SHORT).show()
                }

            }
            requestCode == CAMERA && resultCode == Activity.RESULT_OK -> {
                val thumbnail = data?.extras?.get("data") as Bitmap
                iv_image.setImageBitmap(thumbnail)
                imageBitmap = thumbnail
            }
        }
    }
}
